##  The rate of a multistep enzyme reaction

Numeric analysis and the rate-limiting step. 

![plot](./enzyme_scheme.png)



The **button below** opens an online 
jupyter notebook, where one can play around with parameters

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gbar.dtu.dk%2Fkaysc%2F27302/HEAD?filepath=numerical_sim.ipynb)

